package org.itstep.teststep.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

@Data
public class Group {

    private Long id;
    @NotEmpty
    private String groupName;
    private String formType;
    private List<Long> trainers;
    private List<Long> students;
    private Date startDate;

}
