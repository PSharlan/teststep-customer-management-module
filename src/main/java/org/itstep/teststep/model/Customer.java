package org.itstep.teststep.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
public class Customer {

    private Long id;
    @NotEmpty(message="Username can't be empty")
    private String username;
    @NotEmpty(message="First name can't be empty")
    private String firstName;
    @NotEmpty(message="Last name can't be empty")
    private String lastName;
    @Email(message="Invalid email")
    private String email;
    @NotNull(message="Password can't be null")
    @Size(min=6, message="Password is shorter then 6 characters")
    private String password;
    private String profileImageUrl;
    @NotNull(message="Date of birth can't be empty")
    private Date dateOfBirth;
    private List<Role> roles;
    private Status status;
}
