package org.itstep.teststep.model;

import lombok.Data;

@Data
public class ChangePswRequest {

    //FIXME add validation
    private String email;
    private String oldPassword;
    private String newPassword;
}
