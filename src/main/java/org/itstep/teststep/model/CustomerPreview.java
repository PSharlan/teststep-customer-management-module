package org.itstep.teststep.model;

import lombok.Data;

@Data
public class CustomerPreview {

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String profileImageUrl;

}
