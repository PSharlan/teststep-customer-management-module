package org.itstep.teststep.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class Role {

    private Long id;
    @NotEmpty(message="Name can not be empty")
    private String name;

}
