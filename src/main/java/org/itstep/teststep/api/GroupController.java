package org.itstep.teststep.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.itstep.teststep.entity.GroupEntity;
import org.itstep.teststep.model.Group;
import org.itstep.teststep.service.GroupService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/groups")
@Api(value = "/api/v1/groups", description = "Manage groups")
public class GroupController {

    private final GroupService groupService;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create group", notes = "Required group instance")
    public GroupEntity create(
            @ApiParam(value = "Group instance", required = true)
            @Valid @RequestBody Group group) {
        return groupService.create(group);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Return all existing groups")
    public List<Group> getAllGroups() {
        return groupService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Return group by id")
    public Group getGroupById(
            @ApiParam(value = "Id of a group to lookup for", required = true)
            @PathVariable long id) {
        return groupService.findById(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "Update group", notes = "Required group instance")
    public Group updateGroup(
            @ApiParam(value = "Group instance", required = true)
            @Valid @RequestBody Group group) {
        return groupService.update(group);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete group by id")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteGroup(
            @ApiParam(value = "Id of a group to delete", required = true)
            @PathVariable long id) {
        groupService.delete(id);
    }
}
