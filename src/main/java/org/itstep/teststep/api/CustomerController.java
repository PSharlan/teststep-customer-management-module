package org.itstep.teststep.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.itstep.teststep.model.ChangePswRequest;
import org.itstep.teststep.model.Customer;
import org.itstep.teststep.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/customers")
@Api(value = "/api/v1/customers", description = "Manage customers")
public class CustomerController {

    private CustomerService customerService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Return all existing customers")
    public List<Customer> getAllCustomers() {
        return customerService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Return customer by id")
    public Customer getCustomerById(
            @ApiParam(value = "Id of a customer to lookup for", required = true)
            @PathVariable long id) {
        return customerService.findById(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return customer by email")
    @RequestMapping(value = "/email/{email}", method = RequestMethod.GET)
    public Customer getCustomerByEmail(
            @ApiParam(value = "Customer email", required = true)
            @PathVariable String email) {
        return customerService.findByEmail(email);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Return customer by username")
    @RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
    public Customer getCustomerByUsername(
            @ApiParam(value = "Customer username", required = true)
            @PathVariable String username) {
        return customerService.findByUsername(username);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "Update customer", notes = "Required customer instance")
    public Customer updateCustomer(
            @ApiParam(value = "Customer instance", required = true)
            @Valid @RequestBody Customer customer) {
        return customerService.update(customer);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete customer by id")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteCustomer(
            @ApiParam(value = "Id of a customer to delete", required = true)
            @PathVariable long id) {
        customerService.delete(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/password", method = RequestMethod.PUT)
    @ApiOperation(value = "Change customer password", notes = "Customer email, old and new passwords are required.")
    public Customer changePassword(
            @ApiParam(value = "Request info", required = true)
            @Valid @RequestBody ChangePswRequest req) {
        return customerService.changePassword(req);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}/roles", method = RequestMethod.PUT)
    @ApiOperation(value = "Update customer roles", notes = "Customer id and Set of roles are required.")
    public Customer changeRole(
            @ApiParam(value = "Customer id")
            @PathVariable long id,
            @ApiParam(value = "New role")
            @RequestParam Set<String> roles) {
        return customerService.changeRole(id, roles);
    }
}
