package org.itstep.teststep.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.itstep.teststep.model.Customer;
import org.itstep.teststep.service.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/auth")
@Api(value = "/api/v1/auth", description = "Manage customers registration")
public class RegistrationController {

    private final CustomerService customerService;

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ApiOperation(value = "Create customer", notes = "Required customer instance")
    public Customer register(
            @ApiParam(value = "Customer instance", required = true)
            @Valid @RequestBody Customer customer) {
        return customerService.register(customer);
    }
}
