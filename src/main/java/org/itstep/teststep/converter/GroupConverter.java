package org.itstep.teststep.converter;

import lombok.AllArgsConstructor;
import org.itstep.teststep.entity.GroupEntity;
import org.itstep.teststep.model.Group;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class GroupConverter {

    private ModelMapper modelMapper;

    public Group convertToModel(GroupEntity groupEntity) {
        return modelMapper.map(groupEntity, Group.class);
    }

    public GroupEntity convertToEntity(Group group) {
        return modelMapper.map(group, GroupEntity.class);
    }

    public List<Group> convertToModels(List<GroupEntity> groupEntities) {
        return groupEntities.stream().map(this::convertToModel).collect(Collectors.toList());
    }

    public List<GroupEntity> convertToEntities(List<Group> groups) {
        return groups.stream().map(this::convertToEntity).collect(Collectors.toList());
    }

}
