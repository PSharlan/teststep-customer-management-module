package org.itstep.teststep.converter;

import lombok.AllArgsConstructor;
import org.itstep.teststep.entity.CustomerEntity;
import org.itstep.teststep.model.Customer;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class CustomerConverter {

    private ModelMapper modelMapper;

    public Customer convertToModel(CustomerEntity customerEntity) {
        return modelMapper.map(customerEntity, Customer.class);
    }

    public CustomerEntity convertToEntity(Customer customer) {
        return modelMapper.map(customer, CustomerEntity.class);
    }

    public List<Customer> convertToModels(List<CustomerEntity> customerEntities) {
        return customerEntities.stream().map(this::convertToModel).collect(Collectors.toList());
    }

    public List<CustomerEntity> convertToEntities(List<Customer> customers) {
        return customers.stream().map(this::convertToEntity).collect(Collectors.toList());
    }
}
