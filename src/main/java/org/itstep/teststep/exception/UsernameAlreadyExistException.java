package org.itstep.teststep.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class UsernameAlreadyExistException extends RuntimeException {

    @Getter
    private HttpStatus status;

    public UsernameAlreadyExistException(String message) {
        super(message);
        this.status = HttpStatus.BAD_REQUEST;
    }
}
