package org.itstep.teststep.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class PasswordsNotEqualsException extends RuntimeException{

    @Getter
    private HttpStatus status;

    public PasswordsNotEqualsException(String message) {
        super(message);
        this.status = HttpStatus.BAD_REQUEST;
    }
}
