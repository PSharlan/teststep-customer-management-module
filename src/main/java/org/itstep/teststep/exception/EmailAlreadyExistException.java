package org.itstep.teststep.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class EmailAlreadyExistException extends RuntimeException {

    @Getter
    private HttpStatus status;

    public EmailAlreadyExistException(String message) {
        super(message);
        this.status = HttpStatus.BAD_REQUEST;
    }
}
