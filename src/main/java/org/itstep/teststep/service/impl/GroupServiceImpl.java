package org.itstep.teststep.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.itstep.teststep.converter.GroupConverter;
import org.itstep.teststep.entity.CustomerEntity;
import org.itstep.teststep.entity.GroupEntity;
import org.itstep.teststep.entity.StatusEntity;
import org.itstep.teststep.model.Group;
import org.itstep.teststep.repository.CustomerRepository;
import org.itstep.teststep.repository.GroupRepository;
import org.itstep.teststep.service.GroupService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.itstep.teststep.util.ExceptionUtil.notFoundException;

@Slf4j
@Service
@AllArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final CustomerRepository customerRepository;
    private final GroupRepository groupRepository;
    private final GroupConverter groupConverter;

    @Override
    @Transactional
    public GroupEntity create(Group group) {
        final List<CustomerEntity> trainers = customerRepository.findAllById(group.getTrainers());
        final List<CustomerEntity> students = customerRepository.findAllById(group.getStudents());
        final GroupEntity groupToSave = groupConverter.convertToEntity(group);
        groupToSave.setStatus(StatusEntity.ACTIVE);
        groupToSave.setTrainers(trainers);
        students.forEach(s -> s.setStudentGroup(groupToSave));
        groupToSave.setStudents(students);
        final GroupEntity savedGroup = groupRepository.save(groupToSave);
        log.info("IN create - group: {} successfully created", savedGroup);
        return savedGroup;
    }

    @Override
    @Transactional
    public List<Group> findAll() {
        final List<GroupEntity> foundGroups = groupRepository.findAll();
        log.info("IN findAll - groups: {} were found", foundGroups);
        return groupConverter.convertToModels(foundGroups);
    }

    @Override
    @Transactional
    public Group findByGroupName(String groupName) {
        final GroupEntity foundGroup = groupRepository.findByGroupName(groupName)
                .orElseThrow(() -> notFoundException("Group with name : " + groupName + " not found"));
        log.info("IN findByGroupName - group: {} were found", foundGroup);
        return groupConverter.convertToModel(foundGroup);
    }

    @Override
    @Transactional
    public Group findById(long id) {
        final GroupEntity foundGroup = groupRepository.findById(id)
                .orElseThrow(() -> notFoundException("Group with id : " + id + " not found"));
        log.info("IN findById - group: {} were found", foundGroup);
        return groupConverter.convertToModel(foundGroup);
    }

    @Override
    @Transactional
    public void delete(long id) {
        final GroupEntity foundGroup = groupConverter.convertToEntity(findById(id));
        foundGroup.setStatus(StatusEntity.DELETED);
        final GroupEntity updatedGroup = groupRepository.save(foundGroup);
        log.info("IN delete - group: {} updated with status 'DELETED'", updatedGroup);
    }

    @Override
    @Transactional
    public Group update(Group group) {
        final GroupEntity groupToUpdate = groupConverter.convertToEntity(group);
        final GroupEntity updatedGroup = groupRepository.save(groupToUpdate);
        log.info("IN update - group: {} successfully updated", updatedGroup);
        return groupConverter.convertToModel(updatedGroup);
    }
}
