package org.itstep.teststep.service.impl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.itstep.teststep.converter.CustomerConverter;
import org.itstep.teststep.entity.CustomerEntity;
import org.itstep.teststep.entity.RoleEntity;
import org.itstep.teststep.entity.StatusEntity;
import org.itstep.teststep.model.ChangePswRequest;
import org.itstep.teststep.model.Customer;
import org.itstep.teststep.repository.CustomerRepository;
import org.itstep.teststep.repository.RoleRepository;
import org.itstep.teststep.service.CustomerService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.itstep.teststep.util.ExceptionUtil.*;

@Slf4j
@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final RoleRepository roleRepository;
    private final CustomerConverter customerConverter;
    private final CustomerRepository customerRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public Customer register(Customer customer) {
        checkExistingCustomer(customer);

        final CustomerEntity customerToSave = customerConverter.convertToEntity(customer);
        final RoleEntity role = roleRepository.findByName("USER")
                .orElseThrow(() -> notFoundException("Role 'USER' do not exist"));

        customerToSave.setRoles(Collections.singletonList(role));
        customerToSave.setPassword(passwordEncoder.encode(customer.getPassword()));
        customerToSave.setStatus(StatusEntity.ACTIVE);

        final CustomerEntity savedCustomer = customerRepository.save(customerToSave);
        log.info("IN register - customer: {} successfully registered", savedCustomer);
        return customerConverter.convertToModel(savedCustomer);
    }

    @Override
    @Transactional
    public List<Customer> findAll() {
        final List<CustomerEntity> foundCustomers = customerRepository.findAll();
        log.info("IN find all - {} customer were found", foundCustomers.size());
        return customerConverter.convertToModels(foundCustomers);
    }

    @Override
    @Transactional
    public Customer findByUsername(String username) {
        final CustomerEntity foundCustomer = customerRepository.findByUsername(username)
                .orElseThrow(() -> notFoundException("Customer with username : " + username + " not found"));
        log.info("IN findByUsername - customer: {} found by username: {}", foundCustomer, username);
        return customerConverter.convertToModel(foundCustomer);
    }

    @Override
    @Transactional
    public Customer findById(long id) {
        final CustomerEntity foundCustomer = customerRepository.findById(id)
                .orElseThrow(() -> notFoundException("Customer with id : " + id + " not found"));
        log.info("IN findById - customer: {} found by id: {}", foundCustomer, id);
        return customerConverter.convertToModel(foundCustomer);
    }

    @Override
    @Transactional
    public void delete(long id) {
        final CustomerEntity foundCustomer = customerConverter.convertToEntity(findById(id));
        foundCustomer.setStatus(StatusEntity.DELETED);
        final CustomerEntity updatedCustomer = customerRepository.save(foundCustomer);
        log.info("IN delete - customer: {} updated with status 'DELETED'", updatedCustomer);
    }

    @Override
    @Transactional
    public Customer findByEmail(String email) {
        final CustomerEntity foundCustomer = customerRepository.findByEmail(email)
                .orElseThrow(() -> notFoundException("Customer is not found. Unknown email: " + email));
        log.info("IN findByUsername - customer: {} found by email: {}", foundCustomer, email);
        return customerConverter.convertToModel(foundCustomer);
    }

    @Override
    @Transactional
    public Customer update(Customer customer) {
        final CustomerEntity customerToUpdate = customerConverter.convertToEntity(customer);
        final CustomerEntity updatedCustomer = customerRepository.save(customerToUpdate);
        log.info("IN update - customer: {} successfully updated", updatedCustomer);
        return customerConverter.convertToModel(updatedCustomer);
    }

    @Override
    @Transactional
    public Customer changePassword(ChangePswRequest req) {
        final Customer customer = findByEmail(req.getEmail());
        if (!passwordEncoder.matches(req.getOldPassword(), customer.getPassword()))
            throw passwordsNotEqualsException("Wrong password");
        customer.setPassword(passwordEncoder.encode(req.getNewPassword()));
        log.info("IN changePassword - customer: {} found and password will be changed after update", customer);
        return update(customer);
    }

    @Override
    @Transactional
    public Customer changeRole(long id, Set<String> roles) {
        final CustomerEntity customerToUpdate = customerConverter.convertToEntity(findById(id));
        final List<RoleEntity> foundRoles = roleRepository.findByName(roles);
        customerToUpdate.setRoles(foundRoles);
        log.info("IN changeRole - customer: {} found and roles will be changed after update", customerToUpdate);
        return update(customerConverter.convertToModel(customerToUpdate));
    }

    //FIXME
    private void checkExistingCustomer(Customer customer) {
        customerRepository.findByEmail(customer.getEmail())
                .ifPresent(foundCustomer -> {
                    throw emailAlreadyExistException("Email: " + foundCustomer.getEmail() + " already exist");
                });
        customerRepository.findByUsername(customer.getUsername())
                .ifPresent(foundCustomer -> {
                    throw usernameAlreadyExistException("Username: "
                            + foundCustomer.getUsername() + " already exist");
                });
    }
}
