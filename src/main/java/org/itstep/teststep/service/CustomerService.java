package org.itstep.teststep.service;

import org.itstep.teststep.model.ChangePswRequest;
import org.itstep.teststep.model.Customer;

import java.util.List;
import java.util.Set;

public interface CustomerService {

    Customer register(Customer customer);

    List<Customer> findAll();

    Customer findByUsername(String username);

    Customer findById(long id);

    void delete(long id);

    Customer findByEmail(String email);

    Customer update(Customer customer);

    Customer changePassword(ChangePswRequest req);

    Customer changeRole(long id, Set<String> roles);
}
