package org.itstep.teststep.service;

import org.itstep.teststep.entity.GroupEntity;
import org.itstep.teststep.model.Group;

import java.util.List;

public interface GroupService {

    GroupEntity create(Group group);

    List<Group> findAll();

    Group findByGroupName(String groupName);

    Group findById(long id);

    void delete(long id);

    Group update(Group group);
}
