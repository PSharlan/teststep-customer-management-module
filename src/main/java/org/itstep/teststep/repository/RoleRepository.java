package org.itstep.teststep.repository;

import org.itstep.teststep.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    Optional<RoleEntity> findByName(String name);

    List<RoleEntity> findByName(Set<String> names);
}
