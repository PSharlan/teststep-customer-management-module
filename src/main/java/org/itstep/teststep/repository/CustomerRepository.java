package org.itstep.teststep.repository;

import org.itstep.teststep.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

    Optional<CustomerEntity> findByUsername(String username);

    Optional<CustomerEntity> findByEmail(String email);
}
