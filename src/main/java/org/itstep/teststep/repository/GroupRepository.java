package org.itstep.teststep.repository;

import org.itstep.teststep.entity.CustomerEntity;
import org.itstep.teststep.entity.FormTypeEntity;
import org.itstep.teststep.entity.GroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<GroupEntity, Long> {

    Optional<GroupEntity> findByGroupName(String name);

    List<GroupEntity> findByTrainers(List<CustomerEntity> trainers);
    List<GroupEntity> findByStudents(List<CustomerEntity> students);

    List<GroupEntity> findByFormType(FormTypeEntity type);
}
