package org.itstep.teststep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeststepCustomerManagementMuduleApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeststepCustomerManagementMuduleApplication.class, args);
	}

}
