package org.itstep.teststep.entity;

public enum FormTypeEntity {
    JUNIOR_SCHOOL(), HIGH_SCHOOL(), GENERAL_EDUCATION(), SPECIFIED_EDUCATION()
}
