package org.itstep.teststep.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "role")
@EqualsAndHashCode(callSuper = true) //FIXME
public class RoleEntity extends BaseEntity{

    @Column(name = "name")
    private String name;

    //mappedBy = "roles" -> info about mapping at the CURRENT FIELD CLASS (CustomerEntity) at the field "roles"!!!
    @ToString.Exclude
    @JsonBackReference
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    private List<CustomerEntity> customers;
}
