package org.itstep.teststep.entity;

public enum StatusEntity {
    ACTIVE, NOT_ACTIVE, DELETED
}
