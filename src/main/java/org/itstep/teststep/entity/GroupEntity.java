package org.itstep.teststep.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "`group`")
@EqualsAndHashCode(callSuper = true)
public class GroupEntity extends BaseEntity {

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "form_type")
    @Enumerated(EnumType.STRING)
    private FormTypeEntity formType;

    @ToString.Exclude
    @JsonManagedReference
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "group_trainer", // group_trainer -> intermediate table
            joinColumns = {@JoinColumn(name = "group_id", referencedColumnName = "id")}, // joinColumns -> describe info about CURRENT CLASS (GroupEntity)
            inverseJoinColumns = {@JoinColumn(name = "trainer_id", referencedColumnName = "id")}) // inverseJoinColumns -> describe info about CURRENT FIELD (CustomerEntity)
    private List<CustomerEntity> trainers;

    //FIXME about cascade type and removal
    @ToString.Exclude
    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "studentGroup")
    private List<CustomerEntity> students;

    @Column(name = "start_date")
    private Date startDate;

}
