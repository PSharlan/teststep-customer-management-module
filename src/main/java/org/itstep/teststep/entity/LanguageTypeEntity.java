package org.itstep.teststep.entity;

public enum LanguageTypeEntity {
    JAVA(), HTML(), JS()
}
