package org.itstep.teststep.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "customer")
@EqualsAndHashCode(callSuper = true) //FIXME
public class CustomerEntity extends BaseEntity {

    @Column(name = "username")
    private String username;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "profile_image_url")
    private String profileImageUrl;
    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "group_id")
    private GroupEntity studentGroup;

    //mappedBy = "trainerGroups" -> info about mapping at the CURRENT FIELD CLASS (CustomerEntity) at the field "trainerGroups"!!!
    @JsonBackReference
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "trainers", fetch = FetchType.LAZY)
    private List<GroupEntity> trainerGroups;

    @JsonManagedReference
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "customer_role", // customer_role -> intermediate table
            joinColumns = {@JoinColumn(name = "customer_id", referencedColumnName = "id")}, // joinColumns -> describe info about CURRENT CLASS (CustomerEntity)
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id")}) // inverseJoinColumns -> describe info about CURRENT FIELD (RoleEntity)
    private List<RoleEntity> roles;
}
