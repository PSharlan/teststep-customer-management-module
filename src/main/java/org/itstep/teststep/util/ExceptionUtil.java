package org.itstep.teststep.util;

import lombok.experimental.UtilityClass;
import org.itstep.teststep.exception.EmailAlreadyExistException;
import org.itstep.teststep.exception.NotFoundException;
import org.itstep.teststep.exception.PasswordsNotEqualsException;
import org.itstep.teststep.exception.UsernameAlreadyExistException;

@UtilityClass
public class ExceptionUtil {

    public static NotFoundException notFoundException(String message){
        return new NotFoundException(message);
    }

    public static EmailAlreadyExistException emailAlreadyExistException(String message) {
        return new EmailAlreadyExistException(message);
    }

    public static UsernameAlreadyExistException usernameAlreadyExistException(String message) {
        return new UsernameAlreadyExistException(message);
    }

    public static PasswordsNotEqualsException passwordsNotEqualsException(String message) {
        return new PasswordsNotEqualsException(message);
    }

    public static IllegalArgumentException illegalArgumentException(String message) {
        return new IllegalArgumentException(message);
    }

}
